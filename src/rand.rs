use std::io;
use std::cmp::Ordering;
use rand::Rng;

pub fn guess() {
    println!("----------Welcome To Guessing Game----------");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    println!("Enter any number within 1 to 100:");

    let mut guess = String::new();

    io::stdin().read_line(&mut guess)
       .expect("Something wrong with input!");

    let guess: u32 = guess.trim().parse()
        .expect("it is not a number!");

    println!("secret Number: {}", secret_number);
    println!("You guessed: {}", guess);

    match guess.cmp(&secret_number) {
        Ordering::Less => println!("too small"),
        Ordering::Greater => println!("too high!"),
        Ordering::Equal => println!("yay!!!!"),
    }
}
